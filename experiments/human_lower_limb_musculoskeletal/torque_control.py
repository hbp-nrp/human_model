import rospy
from std_msgs.msg import Float64
from gazebo_msgs.srv import ApplyJointEffort, JointRequest
from sensor_msgs.msg import JointState

rospy.wait_for_service("gazebo/apply_joint_effort")
rospy.wait_for_service("gazebo/clear_joint_forces")


@nrp.MapRobotSubscriber("joint_states", Topic("/human/joint_states", JointState))
@nrp.MapVariable('clm', initial_value=None)
@nrp.MapVariable(
    "applyEffortService", initial_value=None, scope=nrp.GLOBAL
)
@nrp.MapVariable(
    "clearJointService", initial_value=None, scope=nrp.GLOBAL
)
@nrp.Robot2Neuron()
def controller(
        t, clm, joint_states, applyEffortService, clearJointService
):
    """ Controller """
    pass
    # import rospy
    # from std_msgs.msg import Float64
    # from gazebo_msgs.srv import ApplyJointEffort, JointRequest

    # # initialize ros services
    # if applyEffortService.value is None:
    #     applyEffortService.value = rospy.ServiceProxy(
    #         "/gazebo/apply_joint_effort", ApplyJointEffort
    #     )
    # if clearJointService.value is None:
    #     clearJointService.value = rospy.ServiceProxy(
    #         "/gazebo/clear_joint_forces", JointRequest
    #     )

    # #: joint forces applied cumulatively, therefore need clearing
    # #: (helper function)
    # def clearJoints():
    #     for joint in sim.joints:
    #         clearJointService.value(joint)

    # # add joint efforts here:
    # joint_efforts = [
    #     0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    #     0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
    # ]

    # #: update the joints log in container
    # torques = sim.step(joint_states)

    # clientLogger.info(
    #     "Joint positions : {}".format(
    #         np.asarray(sim.container.joints.positions.log)
    #     )
    # )

    # clearJoints()
    # if t > 1:  # otherwise gazebo crashes
    #     for joint, t in torques.items():
    #         applyEffortService.value(
    #             'mouse::' + joint,
    #             t,
    #             rospy.Time(),
    #             rospy.Duration(-1)
    #         )
