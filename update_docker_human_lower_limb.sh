#!/bin/bash

cd ~/tatarama/human_model
echo "Changed directory to : " $PWD
git pull origin master
echo "Git pull successful!"
rm -r $NRP_EXPERIMENTS_DIRECTORY/human_lower_limb_musculoskeletal
echo "Removed human lower limb experiments folder!"
rm -r $NRP_MODELS_DIRECTORY/human_lower_limb
echo "Removed human lower limb models folder!"
cp -r experiments/human_lower_limb_musculoskeletal $NRP_EXPERIMENTS_DIRECTORY
echo "Copied human lower limb experiments folder!"
cp -r models/human_lower_limb $NRP_MODELS_DIRECTORY
echo "Copied human lower limb models folder!"
cd $NRP_MODELS_DIRECTORY
echo "Changed directory to : " $PWD
bash create-symlinks.sh
echo "Created models symlinks"
